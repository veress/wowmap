var levelLabel = "Уровень";
ma_ChooseZone = function(a,e) {return [a,e];}
var map_hierarchy = [[0, LANG.maps_zones, null, [[0, LANG.maps_cosmicmap, ma_ChooseZone.bind(null, -4, false)], [1, LANG.maps_azeroth, ma_ChooseZone.bind(null, -1, false)], [2, LANG.maps_ek, ma_ChooseZone.bind(null, -3, false), ma_AddOptions([1, 3, 4, 8, 10, 11, 12, 28, 33, 38, 40, 41, 44, 45, 46, 47, 51, 85, 130, 139, 267, 1497, 1519, 1537, 3430, 3433, 3487, 4080, 4298, 4714, 4755, 4815, 4922, 5095, 5144, 5145, 5146, 5287, 5339, 5389])], [3, LANG.maps_kalimdor, ma_ChooseZone.bind(null, -6, false), ma_AddOptions([14, 15, 16, 17, 141, 148, 215, 331, 357, 361, 400, 405, 406, 440, 457, 490, 493, 616, 618, 1377, 1637, 1638, 1657, 3524, 3525, 3557, 4709, 5034, 5695, 5733])], [4, LANG.maps_maelstrom, ma_ChooseZone.bind(null, 5630, false), ma_AddOptions([4720, 4737, 5042, 5416, 5630])], [5, LANG.maps_outland, ma_ChooseZone.bind(null, -2, false), ma_AddOptions([3483, 3518, 3519, 3520, 3521, 3522, 3523, 3703])], [6, LANG.maps_northrend, ma_ChooseZone.bind(null, -5, false), ma_AddOptions([65, 66, 67, 210, 394, 495, 2817, 3537, 3711, 4197, 4395, 4742])], [7, LANG.maps_pandaria, ma_ChooseZone.bind(null, -7, false), ma_AddOptions([5785, 5805, 5840, 5841, 5842, 6006, 6134, 6138, 6141, 6142, 6507, 6661, 6757])], [8, LANG.maps_warlords, ma_ChooseZone.bind(null, -8, false), ma_AddOptions([6941, 6755, 6662, 6721, 6722, 6720, 6719, 7083, 6980, 6723])], [9, LANG.maps_legion, ma_ChooseZone.bind(null, -9, false), ma_AddOptions([7334, 7503, 7541, 7543, 7558, 7637])]]], [, LANG.maps_other], [1, LANG.maps_instances, null, [[0, LANG.maps_raids, null, ma_AddOptions(ma_GetInstancesFromMenu(-2))], [1, LANG.maps_dungeons, null, ma_AddOptions(ma_GetInstancesFromMenu(-1), true)], [2, LANG.maps_scenarios, null, ma_AddOptions(ma_GetInstancesFromMenu(-3))], [3, LANG.maps_battlegrounds, null, ma_AddOptions([2597, 3277, 3358, 3820, 4384, 4710, 5031, 5449, 6051, 6126])]]]];
var resultArr = [];
var sIds = [];
var a = {"Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"'","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"'","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"'","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"'","б":"b","ю":"yu"," ":"-"};
var transliterateId = function(word){
    return word.split('').map(function (char) { 
        return a[char] || char; 
    }).join("");
}
var generateSid = function(newItem) {
    currentSid = transliterateId(newItem.label);
    sch = 0;
    newSid = currentSid + "-" + sch;
    while(sIds.includes(newSid)) {
        sch++;
        newSid = currentSid + "-" + sch;
    }
    newItem.sid = newSid;
    sIds.push(newSid);
}
var createSubLevels = function(item) {
    var sids = Mapper.multiLevelZones[item.id];
    var slabels = g_zone_areas[item.id];
    if (sids && sids.length > 0) {
        for (var i = 0; i < sids.length; i++) {
            var newItem = {};
            newItem.id = sids[i].replace(/\-/g,".");
            newItem.type = "location";
            newItem.parent = item.sid;
            if (slabels && slabels.length == sids.length) {
                    newItem.label = slabels[i];
            } else {
                newItem.label = levelLabel + " " + (i);
            }
            generateSid(newItem);
            resultArr.push(newItem);
        }
    }
}
var parseItem = function(item, parentItem) {
    var newItem = {};
    if (parentItem) {
        newItem.parent = parentItem.sid;
    }
    if (item.length >= 2) {
        newItem.label = item[1];
        generateSid(newItem);
    }
    if (item.length == 2) {
        newItem.type = "menuItem";
    }
    if (item.length == 3 || item.length == 4) {
        newItem.label = item[1];
        if (typeof item[2] === "function") {
            newItem.id = item[2].call()[0];
            if (typeof Mapper.multiLevelZones[newItem.id] !== 'undefined' && Mapper.multiLevelZones[newItem.id].length > 0) {
                newItem.type = "levels";
            } else {
                newItem.type = "location";
            }
            
        } else if (item[2] == null) {
            newItem.type = "menuItem";
        }
    }
    resultArr.push(newItem);
    if (newItem.type === "levels") {
        createSubLevels(newItem);
    }
    if (item.length == 4) {
        if (typeof item[3].length !== "undefined") {
            for (var i = 0; i < item[3].length; i++) {
                parseItem(item[3][i], newItem);
            }
        }
    }
}
for (var i = 0; i < map_hierarchy.length; i++) {
    parseItem(map_hierarchy[i], null);
}
var textArea = document.createElement("textarea");
textArea.style.position = 'fixed';
textArea.style.top = "5vh";
textArea.style.left = "5vw";
textArea.style.height = "90vh";
textArea.style.width = "90vw";
textArea.value = JSON.stringify(resultArr, null, 4);
document.body.appendChild(textArea);
textArea.select();