var hs = function() {
    a = window.location.hash.substr(1).split('&');
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
        var p=a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
};

var mapApp = angular.module("mapApp", [])
    .config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('');
        $locationProvider.html5Mode(true);
    }]);;

mapApp.controller("MapController", function MapController($scope){
    $scope.params = {
        "loading" : true,
        "sideBarHide" : false
    };
    $scope.languages = [{
        "id" : "en-US",
        "label" : "English (US)"
    }, {
        "id" : "ru-RU",
        "label" : "Русский"
    }];
    $scope.search = function() {
        $scope.data.searchResults = [];
        if ($scope.data.locationsByLabel && $scope.data.searchString.length > 1) {
            for (var k in $scope.data.locationsByLabel) {
                if ($scope.data.locationsByLabel.hasOwnProperty(k) && k.toLowerCase().indexOf($scope.data.searchString.toLowerCase()) != -1) {
                    $scope.data.searchResults.push($scope.data.locationsByLabel[k]);
                }
            }
        }
    }
    $scope.sidebarToggle = function() {
        $scope.params.sideBarHide = !$scope.params.sideBarHide;
        setTimeout(function(){
            $scope.recalcImgRatio();
            $scope.$apply();
        }, 50);
    }
    $scope.getId = function(idOrSidMap) {
        if (/^([\-\d\.]{1,10})$/.test(idOrSidMap)) {
            return ($scope.data.locationsById.hasOwnProperty(idOrSidMap)) ? idOrSidMap : null;
        }
        if (/^([\-\da-zA-Z]{1,200})$/.test(idOrSidMap)) {
            return ($scope.data.itemsBySid.hasOwnProperty(idOrSidMap)) ? $scope.data.itemsBySid[idOrSidMap].id : null; //return null if not location
        }
        return null;
    }
    $scope.getLangId = function() {
        return $scope.params.lang;
    }
    $scope.getNormalizeLangId = function() {
        if ($scope.getLangId()) {
            return $scope.getLangId().toString().replace(/\-/g, "").toLowerCase();
        }
    }
    $scope.setLang = function(lang) {
        this.params["lang"] = lang;
    }
    $scope.setDefaultLang = function() {
        $scope.setLang("en-US");
    }
    $scope.loadData = function(callback) {
        $.ajax({
            dataType : 'json',
            url: "data/" + $scope.getLangId() + ".json",
            success : function(jsonData) {
                $scope["data"] = jsonData;
                console.log(jsonData);
                if (callback) {
                    callback();
                }
            }
        });
    }
    $scope.getLocById = function(locId) {
        return $scope.data.locationsById[locId];
    }
    $scope.getLocBySid = function(locSid) {
        var loc = $scope.data.itemsBySid[locSid];
        if (loc && loc.type === "location") {
            return loc;
        }
        return null;
    }
    $scope.getLocId = function() {
        return $scope.locId;
    }
    $scope.getNormalizeLocId = function() {
        if ($scope.getLocId()) {
            return $scope.getLocId().toString().replace(/\./g, "-");
        }
    }
    $scope.setLoc = function(idOrSidMap) {
        var id = $scope.getId(idOrSidMap);
        if (id) {
            if ($scope["locId"] !== id) {
                $scope["locId"] = id;
                $scope.showLocationHierarchy();
                $scope.updateBreadcrumbs();
                $scope.clearMarkers();
                console.log($scope.getLocById(id));
            }
        }
    }
    $scope.showLocationHierarchy = function() {
        var item = $scope.data.locationsById[$scope.locId];
        while (item.parentLink) {
            item.parentLink.showChilds = true;
            item = item.parentLink;
        }
    }
    $scope.updateBreadcrumbs = function() {
        $scope.breadcrumbs = [];
        var item = $scope.data.locationsById[$scope.locId];
        while (item.parentLink) {
            $scope.breadcrumbs.unshift(item);
            item = item.parentLink;
        }
        $scope.breadcrumbs.unshift(item);
    }
    $scope.setDefaultLoc = function() {
        $scope.setLoc($scope.data.defaultItem);
    }
    $scope.hashHandle = function() {
        //$scope.clearMeta();
        if (window.location.hash) {
            var hsData = hs();
            if (hsData && hsData.data) {
                var mapData = hsData.data;
                if (/^[\-\d\.a-z]{1,20}$/g.test(mapData)) {
                //Check getting only map id without coords
                    $scope.setLoc(mapData);
                } else if (/^[\-\d\.a-z\:]+$/g.test(mapData)) {
                //Check getting map and coords
                    mapData = mapData.split(":");
                    $scope.setLoc(mapData[0]);
                    $scope.setMarkersRaw(mapData[1]);
                } else {
                    alert("Unexpected data format in the hash of the URL");
                }
            } else {
                $scope.setDefaultLoc();
            }
        } else {
            $scope.setDefaultLoc();
        }
    }
    $scope.init = function() {
        $scope.setDefaultLang();
        $scope.loadData(function(){
            $scope.processData();
            $scope.hashHandle();
            $scope.$apply();
            $scope.params.loading = false;
            $scope.$apply();
        });
        $scope.params.loading = true;
    }
    $scope.setData = function() {
        $scope.loadData(function(){
            $scope.processData();
            $scope.showLocationHierarchy();
            $scope.updateBreadcrumbs();
            $scope.$apply();
            $scope.params.loading = false;
            $scope.$apply();
        });
        $scope.params.loading = true;
    }
    $scope.processData = function() {
        $scope.createMappings();
    }
    $scope.recalcImgRatio = function() {
        var $img = $("img#location-img, div#location-overlay");
        var img = $img.get(0);
        var $imgContainer = $("img#location-img").parent();
        var imageRatio = parseFloat(img.naturalWidth) / parseFloat(img.naturalHeight);
        var containerRatio = parseFloat($imgContainer.width()) / parseFloat($imgContainer.height());
        if (imageRatio > containerRatio) {
            //ширина контейнера, высоту картинки высчитываем
            $img.css("width", $imgContainer.width());
            $img.css("height", (parseFloat($imgContainer.width()) / parseFloat(img.naturalWidth) * parseFloat(img.naturalHeight)) + "px");
        } else {
            //высота контейнера, ширину картинки высчитываем
            $img.css("height", $imgContainer.height());
            $img.css("width", (parseFloat($imgContainer.height()) / parseFloat(img.naturalHeight) * parseFloat(img.naturalWidth)) + "px");
        }
    }
    $scope.mouseMoveHandler = function($event) {
        var relX = parseFloat($event.pageX - $("#location-overlay").offset().left)/parseFloat($("#location-overlay").width())*100.0;
        var relY = parseFloat($event.pageY - $("#location-overlay").offset().top)/parseFloat($("#location-overlay").height())*100.0;
        $scope.mouseCoordX = Math.round(((relX < 0) ? 0 : ((relX >= 99.9) ? 99.9 : relX)) * 10) / 10;
        $scope.mouseCoordY = Math.round(((relY < 0) ? 0 : ((relY >= 99.9) ? 99.9 : relY)) * 10) / 10;
    }
    $scope.createMarkerClick = function($event) {
        var relX = parseFloat($event.pageX - $("#location-overlay").offset().left)/parseFloat($("#location-overlay").width())*100.0;
        var relY = parseFloat($event.pageY - $("#location-overlay").offset().top)/parseFloat($("#location-overlay").height())*100.0;
        var resultPointX = Math.round(((relX < 0) ? 0 : ((relX >= 99.9) ? 99.9 : relX)) * 10) / 10;
        var resultPointY = Math.round(((relY < 0) ? 0 : ((relY >= 99.9) ? 99.9 : relY)) * 10) / 10;
        $scope.createMarker(resultPointX, resultPointY);
    }
    $scope.createMarker = function(cx, cy) {
        $scope.markers.push({"left" : cx, "top" : cy});
        $scope.updateHash();
    }
    $scope.setMarkersRaw = function(rawMarkersString) {
        $scope.clearMarkers();
        var pieces = rawMarkersString.match(/(\d{6})/g);
        if (pieces) {
            for (var i = 0; i < pieces.length; i++) {
                var piece = pieces[i];
                var rawcoords = piece.match(/(\d{3})/g);
                if (rawcoords && rawcoords.length == 2) {
                    $scope.createMarker(parseFloat(rawcoords[0])/10.0, parseFloat(rawcoords[1])/10.0)
                }
            }
        }
    }
    $scope.deleteMarker = function(marker) {
        marker.deleted = true;
        $scope.updateHash();
    }
    $scope.clearMarkers = function($event) {
        $scope.mouseCoordX = 0.0;
        $scope.mouseCoordY = 0.0;
        $scope.markers = [];
        $scope.updateHash();
    }
    $scope.getMarkersString = function() {
        var ms = "";
        if ($scope.markers && $scope.markers.length > 0) {
            for (var i = 0; i < $scope.markers.length; i++) {
                if ($scope.markers[i].deleted !== true) {
                    var msx = "" + parseInt($scope.markers[i].left*10);
                    while (msx.length < 3) {
                        msx = "0" + msx;
                    }
                    var msy = "" + parseInt($scope.markers[i].top*10);
                    while (msy.length < 3) {
                        msy = "0" + msy;
                    }
                    ms += msx + msy;
                }
            }
        }
        return (ms.length <= 0) ? "" : (":" + ms);
    }
    $scope.createMappings = function() {
        $scope.data.searchString = "";
        $scope.data.searchResults = null;
        $scope.data.locationsById = {};
        $scope.data.locationsByLabel = {};
        $scope.data.itemsBySid = {};
        $scope.data.itemsHierarchy = [];
        if($scope.data && $scope.data.items) {
            for (var k in $scope.data.items){
                if ($scope.data.items.hasOwnProperty(k)) {
                    //Set SID map
                    var item = $scope.data.items[k];
                    $scope.data.itemsBySid[item.sid] = item;
                    if (item.type === "location" || item.type === "levels") {
                        $scope.data.locationsById[item.id] = item;
                        $scope.data.locationsByLabel[item.label] = item;
                    }
                    if (item.parent == null) {
                        $scope.data.itemsHierarchy.push(item);
                    }
                }
            }
            //$scope.data.locationsByLabel.sort();
            for (var k in $scope.data.items){
                if ($scope.data.items.hasOwnProperty(k)) {
                    var item = $scope.data.items[k];
                    if (item.parent) {
                        var parent = $scope.data.itemsBySid[item.parent];
                        if (parent) {
                            item["parentLink"] = parent;
                            if (parent.childs) {
                                parent.childs.push(item);
                            } else {
                                parent["childs"] = [item];
                            }
                        }
                    }
                }
            }
        }
        console.log($scope.data);
        $scope.menuAction = function(item, $event) {
            if (item.childs) {
                if (item.showChilds) {
                    item.showChilds = false;
                } else {
                    item["showChilds"] = true;
                }
            } else if (item.type === "location") {
                $scope.showLocation(item);
            }
        }
        $scope.showLocation = function(item, $event) {
            if (item.type === "location") {
                $scope.setLoc(item.id);
                $scope.data.searchString = "";
            } else if (item.type === "levels" && item.childs && item.childs.length > 0) {
                $scope.setLoc(item.childs[0].id);
                $scope.data.searchString = "";
            }
        }
        $scope.updateHash = function() {
            var updatedHash = "";
            if ($scope.locId) {
                updatedHash += "data=" + $scope.locId + $scope.getMarkersString();
            }
            $scope.link = updatedHash;
        }
    }

    $(window).on('hashchange', function() {
        $scope.hashHandle();
    });
    $(window).on("resize", function() {
        $scope.recalcImgRatio();
    });
    $("img#location-img").on('load', function(){
        $scope.recalcImgRatio();
    });
    $scope.init();
});
